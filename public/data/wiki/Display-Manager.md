# Introduction
Installing Venom Linux from official iso it'll launch the session after login, although there some display manager in the repo like `lightdm`, `lxdm` and `sddm`.


## Replacing or adding Display Manager
You can replacing display manager on the fly without rebooting your machine. First install display manager your choice. I use `lxdm` as example.
* install lxdm
```
$ sudo scratch install lxdm
```
* Replace the old display manager or add lxdm service into `DAEMONS` variable in `/etc/rc.conf` to make it start on boot
```
$ sudo vim /etc/rc.conf
```
(at this point you can just reboot)
* stop the previous one then start lxdm in one line command
```
$ sudo /etc/rc.d/foo stop && sudo /etc/rc.d/lxdm start
```
Then you should be welcomed by lxdm login. Make sure you save all your work before doing this. 
