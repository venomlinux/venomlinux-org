# Introduction
You can create your own custom Venom Linux ISO with your customization, then this custom ISO can be use to install into other machine.
`./scripts/builder.sh` script is used in this process.

## Prepare

Install required dependencies
```
sudo scratch install git syslinux squashfs-tools grub-efi dosfstools libisoburn
```

Cloning ports repository:
Main, scripts and virootfs are mandatory. S6 multilib and nonfree optionals
- Main repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/main 
```

- Community repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/community
```

- Xfce repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/xfce
```

- Mate repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/mate
```

- Scripts repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/scripts 
```

- Configurations repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/virootfs 
```

- S6 repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/s6 
```

- Multilib repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/multilib
```

- Nonfree repository
```sh
git clone --depth=1 https://gitlab.com/venomlinux/ports/nonfree 
```

Enter scripts repository:
```sh
cd scripts
```

Modify script's config file:
```sh
cp config.default scripts/config
vim config
```

Fetch Venom Linux rootfs:
- Sysvinit
```sh
sudo ./sysv-rootfs.sh
```
- S6
```sh
sudo ./s6-iso.sh
```

## Customize to your liking (optional)

Copy your config and customization files into `customize`  (virootfs) directory:
> Note: For config files from user directory should be placed into `customize/etc/skel` directory. This configs will automatically copied over when user created.
```sh
mkdir customize
cp [your stuffs] customize/
```

Configure iso login and password, service and etc:
```sh
vim virootfs/root/custom_script.sh
```

## Generate the ISO

Generate iso with needed packages:
- `-zap`  is for cleaning existing working rootfs, then re-prepare rootfs
- `-pkg=` enter your desire package need to include into the iso with comma (,) separated
- `-iso`  is for generating iso
```sh
sudo ./scripts/builder.sh -zap -pkg=pkg1,pkg2,pkg3,pkgN -iso
```
Or you can use the provided scripts `sysv-iso.sh` or `s6-iso.sh` 

Your custom ISO should be created to top directory of ports repository.

## Testing the ISO

You can use qemu to test generated iso. Theres a script I wrote to easily run qemu with decent flags to test the iso.

First, install qemu:
```sh
sudo scratch install qemu
```

Test the iso:
```sh
./run_qemu <path to your iso>
```

Thats all you need to do to create your own custom iso.
