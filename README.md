## Welcome Venom Linux

Venom Linux is a lightweight source-based linux distribution targetting experienced users.

**Current release: 4.0**

[![2022-10-19-090222-1366x768-scrot.png)](https://i.postimg.cc/wvXqZKMw/2022-10-19-090222-1366x768-scrot.png)](https://postimg.cc/BLvfPVxF)

**[Ports Search](https://github.com/venomlinux/ports/blob/repos/scripts/ports.csv)**

## Announcement
```
[13th February 2024]
Venom Linux 20240213 testing isos released
- Added testing isos 
- Added s6 as optional init 
- Added s6 xorg wayaland and base isos (WIP)

[13th December 2023]
Venom Linux 20231216 isos are released
- Added new wayland iso with sway + nwg-shell for a fully wayland enviroment
- Added new base iso as a simple way to install a minimal system
- Multikernel installation

[10th May 2023]
Venom  Linux 20230510 iso is released
- Include the latest toolchain
- Switch back to rolling release

[13th September 2022]
- Venom Linux 4.0 iso is released.
- Please open issue for any bugs.
- Any fixes and improvement are very welcomed, just open PR for your changes.

[12nd November 2021]
- Venom Linux 3.0 iso is released.
- 3.0-RC1 user can migrate it by manually changing version in `/etc/scratchpkg.repo`, `/etc/os-release`, and `/etc/venom-release`.

[19th October 2021]
- Release Candidate ISO for v3.0 is released.
- Some ports might broken or won't build.
- Please open issue for any bugs.
- Any fixes and improvement are very welcomed, just open PR for your changes.
```

## Download
### Rootfs tarballs
- [Latest sysv tarball](https://nc.abetech.es/index.php/s/QX2taybWRBnyLFS)
- [Latest s6 tarball](https://nc.abetech.es/index.php/s/mtpAqi7BBPDXEPB)

### Testing isos
- [Testing xorg (openbox) wayalnd (sway + nwg-shell) base for sysv or s6 init isos](https://nc.abetech.es/index.php/s/P2MBijAgF4Z5GZS)

### Current isos
- [Current xorg (openbox) wayalnd (sway + nwg-shell) base isos](https://nc.abetech.es/index.php/s/n96mosgP9p8dzEX)

### Old isos
- [Archived isos](https://nc.abetech.es/index.php/s/LGFSLDmSCqjDx9C)

### 4.0
- [https://osdn.net/dl/venomlinux/venomlinux-4.0-x86_64-20220912.iso](https://osdn.net/dl/venomlinux/venomlinux-4.0-x86_64-20220912.iso)
- [https://sourceforge.net/projects/venomlinux/files/4.0/venomlinux-4.0-x86_64-20220912.iso](https://sourceforge.net/projects/venomlinux/files/4.0/venomlinux-4.0-x86_64-20220912.iso)
- [https://github.com/venomlinux/ports/releases/download/v4.0/venomlinux-rootfs-4.0-x86_64.tar.xz](https://github.com/venomlinux/ports/releases/download/v4.0/venomlinux-rootfs-4.0-x86_64.tar.xz)

## Goal
- Minimal as possible
- Highly customizable
- No systemd (elogind or any part from it)
- No huge software like Gnome
- Community participating

## Links
Telegram group: [https://t.me/venomlinux](https://t.me/venomlinux)

Telegram updates channel: [https://t.me/venomlinuxchannel](https://t.me/venomlinuxchannel)

Forum: [https://www.reddit.com/r/venomlinux](https://www.reddit.com/r/venomlinux)

Bugs/Issues: [https://github.com/venomlinux/ports/issues](https://github.com/venomlinux/ports/issues)

Wiki: [https://github.com/venomlinux/ports/wiki](https://github.com/venomlinux/ports/wiki)

## Contact
email: <a href = "mailto: emmett1.2miligrams@protonmail.com">emmett1.2miligrams@protonmail.com</a>

github: [https://github.com/emmett1](https://github.com/emmett1)

## Donate
Venom Linux is non profitable project for linux community, if you like this project and want to support me you can make donation so I can keep this project going.

buymeacoffee: [https://www.buymeacoffee.com/venomlinux](https://www.buymeacoffee.com/venomlinux)

paypal: [https://paypal.me/syazwanemmett](https://paypal.me/syazwanemmett)
